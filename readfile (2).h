#define N 1024
#define MAX 64


//Whether to open the file
int open_file(char *file_name){
    if(fopen(file_name, "r") == NULL){
		return -1;
	}
    return 0;
}

struct worker
{
    int ID_number;       //employee  ID_No. (6-digits,100000-999999)
    char first_name[MAX];       //first name,the maximum length is no more than 64
    char last_name[MAX];        //last name
    int salary;                 //salary
};

int read_int(int *variable){
    if(scanf("%d", variable) == 0){
		return -1;
	}
    return 0;
}

int read_float(float *variable){
    if(scanf("%f", variable) == 0){
		return -1;
	}
    return 0;
}

int read_string(char *variable){
    if(scanf("%s", variable) == 0){
		return -1;
	}

    return 0;
}

void close_file(FILE *file_name){
	fclose(file_name);
}

/*
compare specified variables,
it would be used in sort a list.
*/
int comp(const void *p1,const void *p2){
	int id1=((struct worker *)p1)->ID_number;
	int id2=((struct worker *)p2)->ID_number;
	return id1>id2?1:-1;
}

//比较salary
int comp_salary(const void *p1,const void *p2){
	int id1=((struct worker *)p1)->salary;
	int id2=((struct worker *)p2)->salary;
	return id1>id2?1:-1;
}


//binary search
int binary_search(const int arr[],int min,int max,int target){
	if(max>=min){
		int mid=min+(max-min)/2;
		if(arr[mid]==target){
			return mid;
		}else if(arr[mid]>target){
			return binary_search(arr,min,mid-1,target);
		}else{
			return binary_search(arr,mid+1,max,target);
		}
	}
	return -1;
}

//Lookup function, by employee's ID No.
void Bisearch(struct worker workers[N],const int arr[],int min,int max,int target){

    int worker_id;  //define a variable to receive the return value

    printf("Please type in the employee's ID No.:\n");
    scanf("%d",&target);
    //binary search
    if(max>=min){
		int mid=min+(max-min)/2;
		if(arr[mid]==target){
			worker_id= mid;
		}else if(arr[mid]>target){
			worker_id= binary_search(arr,min,mid-1,target);
		}else{
			worker_id= binary_search(arr,mid+1,max,target);
		}
	}else{worker_id=-1;}

    if(worker_id==-1){
        printf("Employee with id %d not found in DB\n",target);
    }else{
		printf("\n %d\t %s\t %s\t %d\t \n ", workers[worker_id].ID_number, workers[worker_id].first_name,
                workers[worker_id].last_name, workers[worker_id].salary);

    }

}


//Look up by last name
int search_lastname(struct worker workers[N],int number){
    char find_lastname[64];
    printf("\n please enter the employee's  last name：");
    read_string(find_lastname);
	int i=0;
	while(i!=number){
		if(strcmp(workers[i].last_name,find_lastname)==0){
			return i;
		}
		i++;
	}
	return -1;
}



//Lookup function, by employee's last name
void  Look_up_by_Lastname(struct worker workers[N],int number,char lastname[])
{
    int key;
    key=search_lastname(workers,number);
    int i=0;
    if(key==-1){
        printf("Employee with lastname %s not found in DB\n",lastname);
    }else{
      printf(" ID_number   first_name     last_name     salary   \n ");
      printf("-----------------------------------------------------\n ");
      printf(" %-10s %-10s %10d %10d \n ", workers[key].first_name, workers[key].last_name,workers[key].ID_number, workers[key].salary);
    }
}




// Search all employees with same last name

void search_all_lastname(struct worker workers[N],int number){
    char find_lastname[64];
    printf("\n please enter the employee's  last name：");
    read_string(find_lastname);
    int worker_id;
    int i=0;
    int k=0;
    printf(" first_name   last_name     ID_number     salary   \n ");
    printf("-------------------------------------------------------\n");
    while(i!=number){
        if(strcasecmp(workers[i].last_name,find_lastname)==0){
                k++;
            printf(" %-10s %-13s %-13d %d \n ", workers[i].first_name, workers[i].last_name,workers[i].ID_number, workers[i].salary);
        }
        i++;
    }
    if (k==0){
        printf("Employee with last name: %s not found in DB\n",find_lastname);
    }

}




// Look up the M highest salary
void Look_up_salary(struct worker workers[N],int number){
    int M;
    struct worker *same =workers;

    printf("What is the number of the highest salaries do you wish to look for?\n");
    read_int(&M);
    // To make sure M>0 and M<=number
    while(1){
        if(M<0 || M>number){
            printf("M is out of range, please re-enter M：\n");
            read_int(&M);
        }else{
            break;
        }
    }
    // Apply quick sort to sort the information
    qsort(same,number,sizeof(same[0]),comp_salary);
    printf("*********************************************************\n");
		printf(" Name                     salary       ID_number\n");
		printf("-----------------------------------------------------\n");
		int o=0;
		while(o<M){
			printf("%-10s %-10s %13d %10d \n",same[number-1-o].first_name,same[number-1-o].last_name,same[number-1-o].salary,same[number-1-o].ID_number);
			o++;
		}

    }






//print the table of employee's information ,display all employee's information and count the total number of employees
void  Display(struct worker workers[N],int number)
{
    printf("\t\t Table Of Employee's Information \n");
    printf("\n first_name   last_name     salary     ID_number    \n ");
    printf("------------------------------------------------------\n" );
    int i=0;
    while (i!=number)
    {
        printf("%-10s %-10s %13d %10d \n",workers[i].first_name,workers[i].last_name,workers[i].salary,workers[i].ID_number);
        i++;

    }
    printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    printf("Number of Employees (%d)\n",number);

}
