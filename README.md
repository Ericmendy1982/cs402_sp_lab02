# README #

Introduction of the System:

1. Purpose to build the system and its functionality:
The system is a brief managing system of employees' information, including staff's ID No., first name and last name, salary. It supports various editing operations to the database of the employees' information, including (1)adding employee's information and sorting the information in increasing order of the ID No., (2)searching information by ID No., or (3)employee's last name, (4) searching all employees with same last name; (5)reading and saving the information in a txt file, (6)changing or deleting an employee's information, (7)print all employee's information; (8) listing the M highest salaries (M is a No. input by the user); etc.

2. Operating System and Programing Language:
The system is programmed in C Language, applicable to both mac and windows system. 

3. Structure:
The program adopts a top-down design, which defines a struct that dominates the system. The data is organized as linked array list. Whether it reads or writes to an external datafile, the information will be formatted in such a way. 

4. Operating Manual:
    # After running the program, a main menu will be displayed on the I/O window, which lists the possible operation to take:
         (1) input 1, to print out the table of employee information; 
         (2) input 2, to search the employee's information by ID No.; After input the target ID No. the information will be presented to the operator; 
         (3) input 3, to search the employee's information by last name; After input the last name, the information will be presented to the operator, however, please be noted that the program has not eliminated the possibility of repetitive last names. 
         (4) input 4, to add new employee's information and save to the database; In the coding, the program makes a decision where to insert the information, so as to maintain the data in increasing order of ID.No;
         (5) input 5,  to exit the system;
         (6) input 6, to delete certain employee's information, to locate the employee's information by his/her ID No.
         (7) input 7, to change certain employee's information, to locate the employee's information by his/her ID No.
         (8) input 8, to look up M highest salaries, the M is a value input by the user;
         (9) input 9, to list all employees with same last name;
         or tells the user that he/she has made a wrong input.

5.  Special note:
         (1) the database can only contains information of no more than 1024 employees;
         (2) the employee ID No. ranges from 100000 to 999999, and it must contain 6 digits;
         (3) the salary must be in a range of $30000 to $150000;
         (4) In order to faster the search by employee's ID No., the coding applies a binary search;
         (5) To open the database, the program will require the user to input the saving path of the datafile;
         (6) The program applies quick sort  to sort out the information as well. 


