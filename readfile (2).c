//
//  main.c
//  employ information system
//
//  Created by 闳生 on 2020/11/2.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile (2).h"

//the main function
int main()
{
    typedef int status;

    FILE *fp;                                    //define a file type
	char file_name[50];
	printf("Please input file name——:\n");
	scanf("%s",file_name);
	if (open_file(file_name)==-1){
		printf("The file is not readable!\n");
		return -1;
	}
	fp=fopen(file_name,"r");


	int number=0;
	int workerList[1024];
	struct worker workers[1024];
	//read the data in the file
	while(!feof(fp)){
		fscanf(fp,"%d \t %s \t %s \t %d\n",&workers[number].ID_number,workers[number].first_name,workers[number].last_name,&workers[number].salary);
		workerList[number]=workers[number].ID_number;
		number++;
	}
	fclose(fp);


    int flag;

    int six_digit_id;                        //define the variable for search by ID No.
    char last_name[64];
    char first_name[64];
    int salary;
    int yes_or_no;
    int del_number; //   define a variable to the ID No. to delete
    int worker_id;

    while(1)
     {
            printf("\n***************************************************\n");
            printf("\tEmployee Information System\n");
            printf("=======================Menu========================\n ");
            printf("1.Print the Table of Employee Information\n ");
            printf("2.Look up by Employee's ID number\n ");
            printf("3.Look up by Employee's Last Name\n ");
            printf("4.Add Employee Information\n ");
            printf("5.Quit\n ");
            printf("6.Remove an employee's information.\n ");
            printf("7.Update an employee's information\n ");
            printf("8.Print the M employees with the highest salaries\n ");
            printf("9.Find all employees with matching last name\n ");
            printf("**************************************************\n");

            printf("Please enter your option：");
            scanf("%d", &flag);
            printf("--------------------------------------------------\n");

            //Apply quicksort to sort out the information by ID No.
            qsort(workers,number,sizeof(workers[0]),comp);
			for(int i=0;i<number;i++){
		 	   workerList[i]=workers[i].ID_number;
			}
            switch (flag)
            {

                case 1:
                    Display(workers,number);
                    break;                              // Print the tale of employee information
                case 2:
                    Bisearch(workers,workerList,0,number,six_digit_id);
                    break;                              // Lookup employee information by ID No.
                case 3:
                    //search_lastname(workers,number);
                    Look_up_by_Lastname(workers,number,last_name);
                    break;                              //Lookup employee information by last name
                case 4:
                   // Add(workers,first_name,last_name,salary,number,workerList);
                   // Save(worker);

                    printf("Enter the first name of the employee: ");
                    read_string(first_name);            //apply the read_string function
                    printf("Enter the last name of the employee: ");
                    read_string(last_name);
                    while(1){
                        printf("Enter employee's salary ($30,000 and $150,000): ");
                        read_int(&salary);
                        printf("Do you really want to add the following information into the database?\n");
                        printf("%s %s , salary: %d\n",first_name,last_name,salary);
                        printf("whether saved to a file？(1-----yes,0-----no！ )");
                        read_int(&yes_or_no);
                            if(yes_or_no==1){
                            if(salary>=30000&&salary<=150000){
                                int cur_id=workers[number-1].ID_number;
                                int new_id=cur_id+1;
                                if(new_id<100000||new_id>999999){
                                    printf("id number is out of bound\n");
                                    break;
                                }else{
                                    strcpy(workers[number].first_name,first_name);
                                    strcpy(workers[number].last_name,last_name);
                                    workers[number].salary=salary;
                                    workers[number].ID_number=new_id;
                                    workerList[number]=workers[number].ID_number;
                                    number++;
                                    printf("adding successfully.\n");
                                    break;
                                }

                            }else{
                                printf("salary invalid, please enter again.\n");
                            }
                        }
                    }

                    break;                                                  //add employee information and save to the file
                case 5:
                    printf(" \nPrompt: Do u really want to quit the system? \n ");
                    exit(0);
                    break;   //Quit the system
                case 6:
                   // Delete(workers,workerList,number);                       // delete some employee information

                    //int yes_or_no;
                    printf("Please enter the ID_number:\n");
                    read_int(&del_number);
                    int flag=binary_search(workerList,0,number,del_number);
                    if(flag==-1){
                        printf("ID No.: %d is not in the database\n",del_number);
                    }else{
                        printf("Do u really want to delete this information(1-----yes,0-----no )");
                        read_int(&yes_or_no);
                        if(yes_or_no==1){
                           for(int i=flag;i<number+1;i++){
                              workers[i]=workers[i+1];
                           }
                           number=number-1;
                           printf("Delete succeeded!\n");
                        }else{
                            printf("Delete Failed!\n");
                           }
                        }
                    break;
                case 7:
                  //  Change(workers,workerList,number);                      // change some employee information
                    printf("Please input the ID_number you want to update:\n");
                    read_int(&six_digit_id);
                    worker_id = binary_search(workerList,0,number,six_digit_id);
                    if(worker_id==-1){
                        printf("ID_number %d not be found in DB",six_digit_id);
                    }else{
                        int flag;
                        while(1){
                            printf("\nWhat information do you wish to change?\n");
                            printf("---------------------------------------\n");
                            printf("To change the first name, enter 1:\n");
                            printf("To change the last name, enter 2:\n");
                            printf("To change the salary, enter 3:\n");
                            printf("Nothing to change,enter 4:\n");

                            printf("Please enter your option:");        // option to choose what information to change
                            scanf("%d",&flag);
                            if(flag==1){
                                 printf(" Please enter the new first name:\n");
                                 scanf("%s",first_name);
                                 strcpy( workers[worker_id].first_name,first_name);
                            }else if(flag==2){
                                 printf(" Please enter the new last name:\n");
                                 scanf("%s",last_name);
                                 strcpy(workers[worker_id].last_name,last_name);
                            }else if(flag==3){
                                 printf(" Please enter the new salary:\n");
                                 scanf("%d", &salary);
                                 workers[worker_id].salary=salary;
                            }else if(flag==4){
                                break;
                            }
                        }
                        printf(" \nPrompt: the information has been updated\n ");
                    }
                    break;
                case 8:
                    Look_up_salary(workers,number);                         // Lookup the M highest salaries
                    break;
                case 9:
                    search_all_lastname(workers, number);                  // Lookup all employees with same last name
                    break;
                default:
                    printf("Prompt: wrong input!\n ");
            }
    }
}

//   D://Desktop/input.txt
